#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import argparse
from numpy import linalg as la
import os
import xyz_py as xyzp


def read_user_args():
    # Read in command line arguments
    description = '''

    Vibrational Projection Analysis (ViPA)

    Projects normal modes of molecule 2 onto those of molecule 1.
    N.B.:
        - Molecule 2 must be larger than molecule 1
        - Molecules must be arranged with common atoms first,
          in the same order and position in space
            e.g. see Benzene and Phenol example in ViPA repository or manual
    '''
    epilog = '''
    The program outputs:\n

    vipa.out
        - Output file containing energies, coefficients,
        and deficit factors.
    vipa_normal_heatmap.png
        - Plot of coefficients for each of the 3N normal modes
        of molecule 2 written in the basis of the normal
        modes of molecule 1
    vipa_vibrational_heatmap.png
        - Plot of coefficients for each of the 3N-6
        vibrational modes of molecule 2 written in the
        basis of the normal modes of molecule 1.
                              '''

    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument(
        'mol_1_fchk',
        type=str,
        help='Gaussian formatted checkpoint file for molecule 1'
    )

    parser.add_argument(
        'mol_2_fchk',
        type=str,
        help='Gaussian formatted checkpoint file for molecule 2'
    )

    parser.add_argument(
        '--mol_1_mass_file',
        type=str,
        default='',
        help='File containing new masses for molecule 1'
    )

    parser.add_argument(
        '--mol_2_mass_file',
        type=str,
        default='',
        help='File containing new masses for molecule 2'
    )

    parser.add_argument(
        '--mol_1_name',
        type=str,
        help='Name of molecule 1 used in plot',
        default="Molecule 1"
    )

    parser.add_argument(
        '--mol_2_name',
        type=str,
        help='Name of molecule 2 used in plot',
        default="Molecule 2"
    )

    parser.add_argument(
        '--show',
        action="store_true",
        help='Display heatmaps on screen'
    )

    parser.add_argument(
        '--coeff_mat',
        action="store_true",
        help='Save coefficients matrix to file'
    )

    user_args = parser.parse_args()

    return user_args


def ismod(val, div):

    if val % div:
        return 1
    else:
        return 0


class Molecule():

    def __init__(self, file_name):

        # Read from fchk, calculated by Gaussian
        self.fchk = file_name
        self.n_atoms = 0
        self.labels = []
        self.coords = []
        self.hess = []
        self.atomic_numbers = []
        self.atomic_masses = []
        self.n_modes = 0
        self.dder = []

        # Calculated within vipa
        self.mode_energies = []
        self.red_masses = []
        self.IT = np.zeros([3, 3])
        self.intensities = []

        return

    def read_fchk(self):
        """
        Reads .fchk file and extracts information
        such as atom masses, coordinates, hessian matrix etc.

        Input:
            file_name (string)  ::  .fchk file name

        Returns:
            None
        """

        # Open .fchk file
        with open(self.fchk, "r") as fchk:
            for line in fchk:
                if "Number of atoms" in line:
                    self.n_atoms = int(line.split()[-1])
                    self.n_modes = self.n_atoms * 3 - 6

                if "Atomic numbers" in line:
                    # Gaussian prints up to 6 atomic numbers per line
                    for n_rows in range(self.n_atoms//6 + ismod(self.n_atoms, 6)):
                        # Move to next line
                        line = next(fchk)
                        [self.atomic_numbers.append(int(i)) for i in line.split()]
                    self.atomic_numbers = np.asarray(self.atomic_numbers)
                    # Convert atomic numbers to atomic labels
                    self.labels = xyzp.num_to_lab(self.atomic_numbers,
                                                  numbered=False)
                    self.formula = xyzp.formdict_to_formstr(
                        xyzp.count_elements(self.labels)
                    )

                if "Real atomic weights" in line:
                    # Gaussian prints up to 5 atomic masses per line
                    for n_rows in range(self.n_atoms//5 + ismod(self.n_atoms, 5)):
                        # Move to next line
                        line = next(fchk)
                        [self.atomic_masses.append(float(i)) for i in line.split()]
                    self.atomic_masses = np.asarray(self.atomic_masses)

                if "Current cartesian coordinates" in line:
                    # Gaussian prints up to 5 coordinates per line
                    # in Bohr! Convert to Angstroms with 0.529177
                    for n_rows in range((3*self.n_atoms)//5 + ismod(3*self.n_atoms, 5)):
                        # Move to next line
                        line = next(fchk)
                        [self.coords.append(float(i)*0.529177) for i in line.split()]
                    # Now rearrange coordinates from a 3N list into a
                    # 3 by N array
                    self.coords = np.reshape(self.coords, (self.n_atoms, 3))

                if "Dipole Derivatives" in line:
                    # Gaussian prints up to 5 derivatives per line
                    for n_rows in range((9*self.n_atoms)//5 + ismod(9*self.n_atoms, 5)):
                        # Move to next line
                        line = next(fchk)
                        [self.dder.append(float(i)) for i in line.split()]
                    # Now rearrange coordinates from a 3N list into a
                    # 3 by N array
                    self.dder = np.reshape(self.dder, (3*self.n_atoms, 3))

                if "Cartesian Force Constants" in line:
                    # Gaussian prints up to 5 elements
                    # of the LOWER/UPPER TRIANGULAR
                    # of the Hessian at a time...
                    hessian_lower = []
                    num_elements = ((3*self.n_atoms)**2 + (3*self.n_atoms))//2
                    for n_rows in range(num_elements//5 + ismod(num_elements, 5)):
                        # Move to next line
                        line = next(fchk)
                        [hessian_lower.append(float(i)) for i in line.split()]

                    # Add hessian_lower to empty Lower Triangular of
                    # Hessian Matrix
                    self.hess = np.zeros([3*self.n_atoms, 3*self.n_atoms])
                    i = 0

                    for j in range(3*self.n_atoms):
                        self.hess[j, :j+1] = hessian_lower[i:j+i+1]
                        i += j + 1

                    # Recover the upper triangular
                    self.hess += np.transpose(self.hess)
                    # Then halve the diagonal to account for adding it twice
                    np.fill_diagonal(self.hess, self.hess.diagonal() * 0.5)

        fchk.close()

        return

    def replace_masses(self, file):
        """
        Replaces masses read from fchk with those read from user provided file

        Input:
            None

        Returns:
            None
        """

        self.atomic_masses = np.loadtxt(file)

        return

    def weight_hessian(self):
        """
        Weights each element of the hessian matrix by sqrt(m1*m2)
        and stores the mass weighted hessian as a new array

        Input:
            None

        Returns:
            None
        """

        # Make array of masses, where each mass is repeated three times
        # and is in atomic units
        # Atomic unit conversion is done by gmol-1 --> kg --> AU
        # Note that the powers of 10 have been simplified on the conversion factor
        # (1.6605389E-27 kg atom^-1)/(9.10938291E-31 kg AU^-1) = (1.6605389/9.10938291)E4 AU atom^-1
        t_mass  = np.tile(self.atomic_masses, (3,1))
        t_mass  = np.reshape(t_mass, 3*self.n_atoms, order='F')
        t_mass *= 1.6605389E4/9.10938291
        t_mass = np.diag(t_mass)

        # # Bohr radius^-2 to angstroms^-2
        # self.hess /= 0.280029

        # # angstroms^-2 to m^-2
        # self.hess *= 10.0E20

        # # Hartree to Joules
        # self.hess *= 4.35974e-18

        # Mass weight the hessian
        # M^1/2 H M^-1/2
        self.hess_mw = la.inv(np.sqrt(t_mass)) @ self.hess @ la.inv(np.sqrt(t_mass))

        return

    def diag_hessian(self):
        """
        Diagonalise the mass weighted hessian

        Input:
            None

        Returns:
            None
        """
        # Diagonalise mass weighted hessian
        self.normal_energies, self.normal_vecs = la.eigh(self.hess_mw)

        # Convert eigenvalues to cm-1
        # catch imaginary frequencies
        sign = []
        for val in self.normal_energies:
            if val < 0.:
                sign.append(-1.)
            else:
                sign.append(1.)

        # Bohr per seconds
        light = 2.99792458 * 2.418884E-7

        self.normal_energies = sign * np.sqrt(np.abs(self.normal_energies))
        self.normal_energies /= (2.*np.pi*light)

        return

    def get_com_coords(self):
        """
        Calculate the centre of mass and define as the origin for the
        atomic coordinates

        Input:
            None

        Returns:
            None
        """

        # Sum of all masses
        mass_sum = np.sum(self.atomic_masses)

        mass_coord_sum = [0, 0, 0]

        for mass, coords in zip(self.atomic_masses, self.coords):
            mass_coord_sum += mass*coords

        self.com = mass_coord_sum/mass_sum

        self.com_coords = self.coords - self.com

        return

    def calc_inertia(self):
        """
        Calcalate the inertia tensor I

        Input:
            None

        Returns:
            None
        """

        # self.com_coords *= 0.529177249

        # Define upper triangular of I
        # I is symmetric
        self.IT[0, 0] = np.sum(self.atomic_masses * (self.com_coords[:, 1]**2 + self.com_coords[:, 2]**2))
        self.IT[0, 1] = -np.sum(self.atomic_masses * self.com_coords[:, 0] * self.com_coords[:, 1])
        self.IT[0, 2] = -np.sum(self.atomic_masses * self.com_coords[:, 0] * self.com_coords[:, 2])
        self.IT[1, 1] = np.sum(self.atomic_masses * (self.com_coords[:, 0]**2 + self.com_coords[:, 2]**2))
        self.IT[1, 2] = -np.sum(self.atomic_masses * self.com_coords[:, 1] * self.com_coords[:, 2])
        self.IT[2, 2] = np.sum(self.atomic_masses * (self.com_coords[:, 0]**2 + self.com_coords[:, 1]**2))

        # Recover lower triangular
        self.IT += np.triu(self.IT, 1).transpose()

        # Convert masses to atomic units
        # self.IT *= 1.6605389E4/9.10938291

        self.IT_vals, self.IT_vecs = la.eigh(self.IT)

        return

    def calc_rot_trans_frame(self):
        """
        Calcalate the transformation matrix D from mass weighted cartesian coordinates
        to internal coordinates. i.e. 3N --> 3N-5/6

        Input:
            None

        Returns:
            None
        """

        # First create a copy of the atomic masses tiled as 
        # m1, m1, m1, m2, m2, m2, m3, m3, m3, ...
        # i.e. 3N copies of each mass
        m = np.tile(self.atomic_masses, (3, 1))
        m = np.reshape(m, 3*self.n_atoms, order='F')

        # Generate translation vectors
        # For D1 this is a N atom copy of the x unit vector
        # 1, 0, 0, 1, 0, 0, 1, 0, 0, ...
        # which is then multiplied by the masses m
        D1 = np.tile(np.array([1, 0, 0]), self.n_atoms) * np.sqrt(m)
        D2 = np.tile(np.array([0, 1, 0]), self.n_atoms) * np.sqrt(m)
        D3 = np.tile(np.array([0, 0, 1]), self.n_atoms) * np.sqrt(m)

        # Generate rotation vectors

        D4 = []
        D5 = []
        D6 = []

        for i in range(self.n_atoms):
            D4.append(np.cross(np.array([1, 0, 0]), self.com_coords[i,:])*np.sqrt(self.atomic_masses[i]))
            D5.append(np.cross(np.array([0, 1, 0]), self.com_coords[i,:])*np.sqrt(self.atomic_masses[i]))
            D6.append(np.cross(np.array([0, 0, 1]), self.com_coords[i,:])*np.sqrt(self.atomic_masses[i]))

        D4 = np.asarray(D4)
        D5 = np.asarray(D5)
        D6 = np.asarray(D6)

        D4 = np.reshape(D4, 3*self.n_atoms, order='C')
        D5 = np.reshape(D5, 3*self.n_atoms, order='C')
        D6 = np.reshape(D6, 3*self.n_atoms, order='C')

        D1 /= np.sqrt(np.dot(D1, D1))
        D2 /= np.sqrt(np.dot(D2, D2))
        D3 /= np.sqrt(np.dot(D3, D3))
        D4 /= np.sqrt(np.dot(D4, D4))
        D5 /= np.sqrt(np.dot(D5, D5))
        D6 /= np.sqrt(np.dot(D6, D6))

        # Make matrix D which is identity
        self.D = np.eye(self.n_atoms*3)

        # Swap out the first 6 columns of D for pure translations and rotations
        self.D[:, 0] = D1
        self.D[:, 1] = D2
        self.D[:, 2] = D3
        self.D[:, 3] = D4
        self.D[:, 4] = D5
        self.D[:, 5] = D6

        # Remaining columns of D are now "trial vectors"
        # Use gram-schmidt orthogonalisation to make these remaining 3n-6 vectors
        # orthogonal to D1->D6
        self.D, r = la.qr(self.D)

        # Transform Hessian to internal coordinates (rotating and translating frame)
        hessian_int = self.D.transpose() @ self.hess_mw @ self.D

        # Diagonalise 3N-6 submatrix of hessian to give vibrational frequencies and
        # normal mode vectors
        self.vib_energies, self.vib_vecs = la.eigh(hessian_int[6:,6:])
        _, self.vib_vecsf = la.eigh(hessian_int)

        # Convert eigenvalues to cm-1
        # catch imaginary frequencies
        sign = []
        for val in self.vib_energies:
            if val < 0.:
                sign.append(-1.)
            else:
                sign.append(1.)

        # Bohr per seconds
        light = 2.99792458 * 2.418884E-7

        # Convert energies to cm-1
        self.vib_energies = sign * np.sqrt(np.abs(self.vib_energies)) / (2.*np.pi*light)

        return

    def calc_red_masses(self):
        '''
        Calculates reduced masses as defined in the Gaussian whitepaper

        Positional arguments:
            None

        Keyword arguments:
            None

        Returns:
            None

        '''

        # Diagonal matrix of M^-1/2. Gaussian whitepaper calls this M
        triple_mass = np.tile(self.atomic_masses, (3, 1))
        triple_mass = np.reshape(triple_mass, 3*self.n_atoms, order='F')
        triple_mass = np.sqrt(triple_mass)
        triple_mass = 1/triple_mass
        triple_mass = np.diag(triple_mass)

        # L_CART = MDL
        self.LCART = triple_mass @ self.D @ self.vib_vecsf

        # Reduced masses - mu
        self.red_masses = 1./(np.sum(self.LCART**2, axis=0))

        # Scale LCART by normalisation factor = sqrt(mu)
        for it in range(self.n_modes):
            self.LCART[:, it] *= np.sqrt(self.red_masses[it])

        return

    def calc_force_constants(self):
        '''
        Calculates reduced masses as defined in the Gaussian whitepaper

        Positional arguments:
            None

        Keyword arguments:
            None

        Returns:
            None
        '''

        # meters per second
        light = 2.99792458E8

        self.force_constants = 4. * np.pi**2
        self.force_constants *= self.vib_energies**2
        self.force_constants *= light**2 * self.red_masses[6:]
        self.force_constants *= 100.*1.66054E-27

        return


def print_output_file(user_args, mol1, mol2, normal_coeffs,
                      vibrational_coeffs):
    """
    Writes information on molecules and projection analysis to output file

    Input:
        mol1(Molecule object) :: Molecule object for first molecule (basis)
        mol2(Molecule object) :: Molecule object for second molecule
        normal_coeffs (np.array) :: Array containing coefficients from
                                    projection of molecule 2's NM
                                    onto molecule 1 NM

    Returns:
        None
    """

    def write_logo_info(f):

        # Write some artwork adapted from
        # https://www.asciiart.eu/animals/reptiles/snakes
        f.write("                             _______\n") # noqa
        f.write("                            / _   _ \ \n") # noqa
        f.write("                           / (.) (.) \ \n") # noqa
        f.write("                          ( _________ )\n") # noqa
        f.write("                           \`-V-|-V-'/\n") # noqa
        f.write("                            \   |   /\n") # noqa
        f.write("                             \  ^  /\n") # noqa
        f.write("                              \    \ \n") # noqa
        f.write("                               -_    -_\n") # noqa
        f.write("                                 -_    -_\n") # noqa
        f.write("                                 _-    _-\n") # noqa
        f.write("                               _-    _-\n") # noqa
        f.write("                             _-    _-\n") # noqa
        f.write("                             -_    -_\n") # noqa
        f.write("                               -_    -_\n") # noqa
        f.write("                                 -_    -_\n") # noqa
        f.write("                                   _-    _-\n") # noqa
        f.write("                          ,-=:_-_-_-__ _-_-_-_:=-.\n") # noqa
        f.write("                         /=I=       ViPA        =I=\ \n") # noqa
        f.write("                        /=I=     Vibrational     =I=\ \n") # noqa
        f.write("                       |=I=I     Projection      =I=|\n") # noqa
        f.write("                       |=I=I      Analysis       =I=|\n") # noqa
        f.write("                        \=I=I=I=I=I=I=I=I=I=I=I=I=I=/\n") # noqa
        f.write("                         \=I=I=I=I=I=I=I=I=I=I=I=I=/\n") # noqa
        f.write("                          \=I=  Jon Kragskow   =I=/\n") # noqa
        f.write("                           \=I=I=I=I=I=I=I=I=I=I=/\n") # noqa
        f.write("                            \=I=I=I=I=I=I=I=I=I=/\n") # noqa
        f.write("                             `================='\n") # noqa


        f.write("********************************************************************************\n") # noqa
        f.write("*           ViPA is a recreation of the work of Grafton and Wheeler:           *\n") # noqa
        f.write("*    A. K. Grafton and R. A. Wheeler, J. Comput. Chem., 1998, 19, 1663–1674    *\n") # noqa
        f.write("********************************************************************************\n") # noqa
        f.write("\n")

        return

    def write_input_file_info(f):

        # Information on input files
        f.write("********************************************************************************\n") # noqa
        f.write("****                              Input Files                               ****\n") # noqa
        f.write("********************************************************************************\n") # noqa
        f.write("\n")
        f.write("Basis molecule read from:\n")
        f.write("{}\n\n".format(mol1.fchk))
        f.write("Projection molecule read from:\n")
        f.write("{}\n".format(mol2.fchk))
        f.write("\n")

        return

    def write_mol_header(f, mol):

        if mol == 1:

            f.write("********************************************************************************\n") # noqa
            f.write("****                     Molecule 1 (basis) Information                     ****\n") # noqa
            f.write("********************************************************************************\n") # noqa
            f.write("\n")
            f.write("The normal modes of this molecule define a basis onto which the normal modes of \n") # noqa
            f.write("                         molecule 2 shall be projected                          \n") # noqa
            f.write("\n")

        else:

            f.write("********************************************************************************\n") # noqa
            f.write("****                  Molecule 2 (projection) Information                   ****\n") # noqa
            f.write("********************************************************************************\n") # noqa
            f.write("\n")
            f.write("The normal modes of this molecule will be projected onto the basis defined by   \n") # noqa
            f.write("                         the normal modes of molecule 1                         \n") # noqa
            f.write("\n")

        return

    def write_mol_info(f, mol):

        f.write("\n")
        f.write("Number of atoms : {:d}\n".format(mol.n_atoms))
        f.write("Number of normal modes : {:d}\n".format(mol.n_atoms * 3))
        f.write("Number of vibrational modes : {:d}\n".format(mol.n_modes))
        f.write("\n")

        f.write("Eigenvalues of inertia tensor (g mol⁻¹ Å²)\n")
        f.write("‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n")

        for val in mol.IT_vals:
            f.write("{:11.6f}\n".format(val))
        f.write("\n")

        f.write("Normal mode frequencies (cm⁻¹)\n")
        f.write("‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n")
        f.write("MW Hessian eigenvalues \n\n")

        for val in mol.normal_energies:
            f.write("{:11.6f}\n".format(val))
        f.write("\n")

        f.write("Vibrational mode frequencies (cm⁻¹)\n")
        f.write("‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n")
        f.write("MW Hessian eigenvalues without translation and rotation \n\n")

        for val in mol.vib_energies:
            f.write("{:11.6f}\n".format(val))
        f.write("\n")

        f.write("Reduced Mass (AMU)\n")
        f.write("‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n")

        f.write("Defined according to Gaussian whitepaper\n\n")
        for rmass in mol.red_masses[6:]:
            f.write("{:10.7f} \n".format(rmass))

        return

    def write_proj_info(f, mode_type, coeffs, mol1_energies, mol2_energies, mol2_natoms):

        f.write("********************************************************************************\n") # noqa
        if mode_type == "vibrational":
            f.write("****                 Vibrational mode projection information                ****\n") # noqa
        else:
            f.write("****                   Normal mode projection information                   ****\n") # noqa
        f.write("********************************************************************************\n") # noqa
        f.write("\n")
        f.write("           Coefficients c in the linear expansion of the normal modes of        \n") # noqa
        if mode_type == "vibrational":
         f.write("             molecule 2 in terms of the vibrational modes of molecule 1        \n") # noqa
        else:
            f.write("              molecule 2 in terms of the normal modes of molecule 1             \n") # noqa
        f.write("\n")
        f.write("Percentage Contributions\n")
        f.write("‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n")

        f.write("Defined as % = 100 * c² \n")
        f.write("Only values higher than 5% are shown\n\n")

        for mit2, val2 in enumerate(mol2_energies):
            f.write("Mode {:3d} : E = {:9.4f} cm⁻¹ : \n".format(mit2 + 1, val2))
            f.write("‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n")
            for mit1, val1 in enumerate(mol1_energies):
                if coeffs[mit2, mit1]**2 * 100. > 5.:
                    f.write("Mode {:3d} : E = {:9.4f} cm⁻¹ : {:7.3f} % \n".format(mit1 + 1, val1, coeffs[mit2, mit1]**2 * 100.))
            f.write('\n')

        f.write("Deficit Factor\n")
        f.write("‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n")

        for mit2, val2 in enumerate(mol2_energies):
            if mit2 == mol2_natoms*3:
                break
            f.write("Mode {:3d} : E = {:9.4f} cm⁻¹ : d = {:7.3f}\n".format(mit2 + 1, val2, (100.-np.sum(coeffs[mit2,:]**2)*100.)))

        return

    def write_masses(f, mol):
        f.write("Masses\n")
        f.write("‾‾‾‾‾‾\n")

        f.write("The following alternative atomic masses were read from file: \n") # noqa
        for mass in mol.atomic_masses:
            f.write("{:15.7f} \n".format(mass))

        f.write("\nThe file {}.gfrq will be generated for this molecule \n"
                .format(os.path.splitext(mol1.fchk)[0]))

        return

    with open("vipa.out", "w", encoding="utf-8") as f:

        write_logo_info(f)

        write_input_file_info(f)

        write_mol_header(f, 1)

        if user_args.mol_1_mass_file:
            write_masses(f, mol1)

        write_mol_info(f, mol1)

        write_mol_header(f, 2)

        if user_args.mol_2_mass_file:
            write_masses(f, mol2)

        write_mol_info(f, mol2)

        f.write("\n")

        write_proj_info(f, 'normal', normal_coeffs, mol1.normal_energies,
                        mol2.normal_energies, mol2.n_atoms)

        write_proj_info(f, 'vibrational', vibrational_coeffs,
                        mol1.vib_energies, mol2.vib_energies, mol2.n_atoms)

        f.write("\n\n********************************************************************************\n") # noqa

        f.close()
    return


def project_modes(user_args, molecule, basis, mode_type):
    """
    Project the modes of molecule onto the modes of basis

    Input:
        molecule (Molecule object) :: molecule whose normal modes are projected
                                      onto those of basis
        basis (Molecule object)    :: molecule whose normal modes are used
                                      as a basis for the normal modes
                                      of "molecule"
        mode_type (string)         :: specifies whether normal or vibrational
                                      modes are used for the projection

    Returns:
        coeffs (numpy array)       :: Coefficients of linear combination
                                      obtain from projection
    """

    more_modes = np.abs(molecule.n_modes - basis.n_modes)

    # Project normal modes of molecule onto those of basis
    # Expand normal modes of basis so that they are the same size of molecule
    # This relies on the coordinates of molecule being ordered correctly,
    # such that the "common" atoms/positions appear at the top, and the
    # "extra" atoms/positions are at the bottom

    if mode_type == "normal":
        big_basis_vecs = np.vstack(
            [basis.normal_vecs, np.zeros([more_modes, basis.n_atoms*3])]
        )
        coeffs = np.dot(molecule.normal_vecs.transpose(), big_basis_vecs)

    if mode_type == "vibrational":
        big_basis_vecs = np.vstack(
            [basis.vib_vecs, np.zeros([more_modes, basis.n_atoms*3-6])]
            )
        coeffs = np.dot(molecule.vib_vecs.transpose(), big_basis_vecs)

    if user_args.coeff_mat:
        np.savetxt("{}_coeffs.dat".format(mode_type), coeffs)
        print("{} mode projection coefficient matrix saved to {}_coeffs.dat"
              .format(mode_type, mode_type))

    return coeffs


def plot_heatmap(user_args, num_modes_b, num_modes_p, coeffs, mode_type):

    fig, ((ax1, ax3), (ax2, ax4)) = plt.subplots(
        2, 2, figsize=(5.05, 5),
        num="ViPA {} mode coefficients".format(mode_type),
        gridspec_kw={
            "height_ratios": [1.25, 0.025], "width_ratios": [1, 1/num_modes_b]
            }
        )

    fig.delaxes(ax4)

    # Plot heatmap of contributions
    x = np.arange(1, num_modes_b+2) - 0.5
    y = np.arange(1, num_modes_p+2) - 0.5

    pcm = ax1.pcolor(x, y, coeffs**2*100, vmin=0, vmax=100)
    ax1.set_ylim(ax1.get_ylim()[::-1])

    ax1.set_ylabel(r"{}".format(user_args.mol_2_name))
    ax1.set_xlabel(r"{}".format(user_args.mol_1_name))

    ax1.xaxis.tick_top()
    ax1.xaxis.set_label_position("top")

    # Add colour bar with same map as plot and 5 ticks
    fig.colorbar(
        orientation='horizontal',
        mappable=pcm,
        cax=ax2,
        ticks=np.linspace(0, 100, 5),
        format='% .f'
    )
    ax2.set_xlabel(r'$c_{ji}^\%$')

    # Add heatmap of % accountancy
    accounted = np.array(
        [np.sum(np.flipud(coeffs)**2*100, axis=1)]
    ).transpose()
    pcm = ax3.pcolor([-0.5, 0.5], y, accounted, vmin=0, vmax=100)

    ax3.set_ylabel('$\\Sigma_{j}c_{ji}$')
    ax3.yaxis.set_label_position("right")

    ax3.set_xticks([])
    ax3.set_yticks([])

    # Adjust and show
    fig.tight_layout()
    # if user_args.show : plt.show()
    plt.savefig('vipa_{}_heatmap.png'.format(mode_type), dpi=500)
    plt.close(fig)

    return


def create_gfrq(mol):
    '''
    Creates gaussian .gfrq using information in mol
    This file can be visualised with gaussview

    Positional arguments:
        mol (Molecule object) : Molecule object containing
                                information on vibrational modes

    Keyword arguments:
        None

    Returns:
        None

    '''

    gfrq_f_name = "{}.gfrq".format(os.path.splitext(mol.fchk)[0])

    with open(gfrq_f_name, 'w') as f:

        f.write(" ---------------------------------------------------------------------\n")
        f.write(" Center     Atomic                     Coordinates (Angstroms)       \n")
        f.write(" Number     Number                        X           Y           Z  \n")
        f.write(" ---------------------------------------------------------------------\n")
        for it, (num, trio), in enumerate(zip(mol.atomic_numbers, mol.coords)):
            f.write("{:7d} {:10d}                   {:9.6f}   {:9.6f}   {:9.6f}\n".format(it+1, num, *trio))
        f.write(" ---------------------------------------------------------------------\n")
        for it, (num, mass), in enumerate(zip(mol.atomic_numbers, mol.atomic_masses)):
            f.write(" Atom {:5d} has atomic number {:3d} and mass {:9.5f}\n".format(it+1, num, mass))
        f.write(" Stoichiometry    {}\n".format(mol.formula))
        f.write(" Framework group  C1[X({})]\n".format(mol.formula))
        f.write(" Deg. of freedom    {:d}\n".format(mol.n_modes))
        f.write(" Full point group                 C1      NOp   1\n")
        f.write(" Full mass-weighted force constant matrix:\n")
        f.write(" Low frequencies ---  {:9.4f}  {:9.4f}  {:9.4f}  {:9.4f}  {:9.4f}   {:9.4f} \n".format(*mol.normal_energies[:6]))
        f.write(" Low frequencies ---  {:9.4f}  {:9.4f}  {:9.4f}\n".format(*mol.normal_energies[6:10]))
        f.write(" Diagonal vibrational polarizability:\n")
        f.write("        0.2883251       0.2884292       4.3467355\n")  # don't know how to get these
        f.write(" Harmonic frequencies (cm**-1), IR intensities (KM/Mole), Raman scattering\n")
        f.write(" activities (A**4/AMU), depolarization ratios for plane and unpolarized\n")
        f.write(" incident light, reduced masses (AMU), force constants (mDyne/A),\n")
        f.write(" and normal coordinates:\n")
        triple_nums = np.tile(mol.atomic_numbers, (3, 1))
        triple_nums = np.reshape(triple_nums, 3*mol.n_atoms, order='F')
        for it in range(0,mol.n_modes - mol.n_modes % 3, 3):
            f.write("                    {:>3d}                    {:>3d}                    {:>3d}\n".format(it+1, it+2, it+3))
            f.write("                      A                      A                      A\n")
            f.write(" Frequencies --   {:9.4f}              {:9.4f}              {:9.4f}\n".format(*mol.vib_energies[it:it+3]))
            f.write(" Red. masses --   {:9.4f}              {:9.4f}              {:9.4f}\n".format(*mol.red_masses[it+6:it+9]))
            f.write(" Frc consts  --   {:9.4f}              {:9.4f}              {:9.4f}\n".format(*mol.force_constants[it:it+3]))
            f.write(" IR Inten    --   {:9.4f}              {:9.4f}              {:9.4f}\n".format(*mol.intensities[it:it+3]))  # don't know how to get these
            f.write("  Atom  AN      X      Y      Z        X      Y      Z        X      Y      Z\n")
            for jt in range(0,3*mol.n_atoms,3):
                f.write("   {:>3d}  {:>2d}    {:>5.2f}  {:>5.2f}  {:>5.2f}    {:>5.2f}  {:>5.2f}  {:>5.2f}    {:>5.2f}  {:>5.2f}  {:>5.2f}\n".format(
                            jt//3 + 1, int(triple_nums[jt]), *mol.LCART[jt:jt+3, it+6],*mol.LCART[jt:jt+3,it+6+1],*mol.LCART[jt:jt+3,it+6+2]))

            # Write final two modes on separate line if needed
            if mol.n_modes % 3 == 2:
                f.write("                    {:>3d}                    {:>3d}\n".format(mol.n_modes-1, mol.n_modes))
                f.write("                      A                      A\n")
                f.write(" Frequencies --   {:9.4f}              {:9.4f}\n".format(*mol.vib_energies[-2:]))
                f.write(" Red. masses --   {:9.4f}              {:9.4f}\n".format(*mol.red_masses[-2:]))
                f.write(" Frc consts  --   {:9.4f}              {:9.4f}\n".format(*mol.force_constants[-2:]))
                f.write(" IR Inten    --   {:9.4f}              {:9.4f}\n".format(*mol.intensities[-2:]))# don't know how to get these
                f.write("  Atom  AN      X      Y      Z        X      Y      Z\n")
                for jt in range(0,3*mol.n_atoms,3):
                    f.write("   {:>3d}  {:>2d}    {:>5.2f}  {:>5.2f}  {:>5.2f}    {:>5.2f}  {:>5.2f}  {:>5.2f}\n".format(
                                jt//3 + 1, int(triple_nums[jt]), *mol.LCART[jt:jt+3, -2], *mol.LCART[jt:jt+3, -1]))

            # Write final mode on separate line if needed
            if mol.n_modes % 3 == 1:
                f.write("                    {:>3d}\n".format(mol.n_modes))
                f.write("                      A                      A\n")
                f.write(" Frequencies --   {:9.4f}\n".format(mol.vib_energies[-1]))
                f.write(" Red. masses --   {:9.4f}\n".format(mol.red_masses[-1]))
                f.write(" Frc consts  --   {:9.4f}\n".format(mol.force_constants[-1]))
                f.write(" IR Inten    --   {:9.4f}\n".format(*mol.intensities[-1]))  # don't know how to get these
                f.write("  Atom  AN      X      Y      Z\n")
                for jt in range(0, 3*mol.n_atoms, 3):
                    f.write("   {:>3d}  {:>2d}    {:>5.2f}  {:>5.2f}  {:>5.2f}\n".format(jt//3 + 1, int(triple_nums[jt]), *mol.LCART[jt:jt+3,-1]))


        # Write some fake thermochem data
        f.write("\n -------------------\n") # noqa
        f.write(" - Thermochemistry -\n") # noqa
        f.write(" -------------------\n") # noqa
        f.write(" Temperature     2.000 Kelvin.  Pressure   1.00000 Atm.\n") # noqa
        f.write(" Thermochemistry will use frequencies scaled by 0.8929.\n") # noqa
        f.write(" Molecular mass:    78.04695 amu.\n") # noqa
        f.write(" Principal axes and moments of inertia in atomic units:\n") # noqa
        f.write("                           1         2         3\n") # noqa
        f.write("     Eigenvalues --   317.28790 317.29699 634.58489\n") # noqa
        f.write("           X            0.99915   0.04126   0.00000\n") # noqa
        f.write("           Y           -0.04126   0.99915   0.00000\n") # noqa
        f.write("           Z            0.00000   0.00000   1.00000\n") # noqa
        f.write(" This molecule is an asymmetric top.\n") # noqa
        f.write(" Rotational symmetry number  1.\n") # noqa
        f.write(" Rotational temperatures (Kelvin)      0.27298     0.27297     0.13649\n") # noqa
        f.write(" Rotational constants (GHZ):           5.68802     5.68786     2.84397\n") # noqa
        f.write(" Zero-point vibrational energy     236187.5 (Joules/Mol)\n") # noqa
        f.write("                                   56.45018 (Kcal/Mol)\n") # noqa
        f.write(" Vibrational temperatures:    532.58   532.73   798.76   798.78   890.12\n") # noqa
        f.write("          (Kelvin)            921.92  1109.31  1109.36  1243.91  1244.02\n") # noqa
        f.write("                             1297.58  1310.26  1311.43  1373.24  1373.36\n") # noqa
        f.write("                             1522.55  1551.19  1551.22  1743.32  1781.37\n") # noqa
        f.write("                             1967.15  1967.25  2127.74  2127.87  4081.71\n") # noqa
        f.write("                             4094.02  4094.15  4114.27  4114.41  4128.02\n") # noqa
        f.write(" \n") # noqa
        f.write(" Zero-point correction=                           0.089959 (Hartree/Particle)\n") # noqa
        f.write(" Thermal correction to Energy=                    0.089978\n") # noqa
        f.write(" Thermal correction to Enthalpy=                  0.089984\n") # noqa
        f.write(" Thermal correction to Gibbs Free Energy=         0.089905\n") # noqa
        f.write("                     E (Thermal)             CV                S\n") # noqa
        f.write("                      KCal/Mol        Cal/Mol-Kelvin    Cal/Mol-Kelvin\n") # noqa
        f.write(" Total                   56.462              5.962             24.860\n") # noqa
        f.write(" Electronic               0.000              0.000              0.000\n") # noqa
        f.write(" Translational            0.006              2.981             14.117\n") # noqa
        f.write(" Rotational               0.006              2.981             10.743\n") # noqa
        f.write(" Vibrational             56.450              0.000              0.000\n") # noqa
        f.write("                       Q            Log10(Q)             Ln(Q)\n") # noqa
        f.write(" Total Bot                                 -Inf              -Inf\n") # noqa
        f.write(" Total V=0                             3.695921          8.510172\n") # noqa
        f.write(" Vib (Bot)                                 -Inf              -Inf\n") # noqa
        f.write(" Vib (V=0)                             0.000000          0.000000\n") # noqa
        f.write(" Electronic      0.100000D+01          0.000000          0.000000\n") # noqa
        f.write(" Translational   0.998797D+02          1.999477          4.603966\n") # noqa
        f.write(" Rotational      0.497100D+02          1.696444          3.906206\n") # noqa

    return

if __name__ == "__main__":

    user_args = read_user_args()

    mol1 = Molecule(user_args.mol_1_fchk)
    mol2 = Molecule(user_args.mol_2_fchk)

    mol1.read_fchk()
    mol2.read_fchk()

    if user_args.mol_1_mass_file:
        mol1.replace_masses(user_args.mol_1_mass_file)
    if user_args.mol_2_mass_file:
        mol2.replace_masses(user_args.mol_2_mass_file)

    mol1.weight_hessian()
    mol2.weight_hessian()

    mol1.diag_hessian()
    mol2.diag_hessian()

    normal_coeffs = project_modes(user_args, mol2, mol1, 'normal')
    plot_heatmap(user_args, mol1.n_atoms*3, mol2.n_atoms*3, normal_coeffs, 'normal')

    mol1.get_com_coords()
    mol2.get_com_coords()

    mol1.calc_inertia()
    mol2.calc_inertia()

    mol1.calc_rot_trans_frame()
    mol2.calc_rot_trans_frame()

    mol1.calc_red_masses()
    mol2.calc_red_masses()

    mol1.intensities = la.norm(mol1.dder, axis=1)
    mol2.intensities = la.norm(mol2.dder, axis=1)

    mol1.calc_force_constants()
    mol2.calc_force_constants()

    vibrational_coeffs = project_modes(user_args, mol2, mol1, 'vibrational')
    plot_heatmap(user_args, mol1.n_atoms*3-6, mol2.n_atoms*3-6, vibrational_coeffs, 'vibrational')

    print_output_file(user_args, mol1, mol2, normal_coeffs, vibrational_coeffs)

    create_gfrq(mol1)
    create_gfrq(mol2)
