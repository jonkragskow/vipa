!Jon Kragskow

PROGRAM ViPa
    USE matrix_tools
    IMPLICIT NONE
    REAL(KIND = 8), ALLOCATABLE :: MBFrequencies(:), MBVectors(:,:), MPFrequencies(:), MPvectors(:,:), Coeff(:,:), MBHessian(:,:), &
                                   MPHessian(:,:), MBMasses(:), MPMasses(:), deficit(:), ApproxProjVectors(:,:),Cosdeficit(:), &
                                   Renormalised_Coeff(:,:), angles(:), massmat(:,:)
    REAL(KIND = 8), PARAMETER   :: pi = 3.14159265_8, c = 2.99792458_8 * 2.418884E-7
    INTEGER                     :: NProjAtoms, NProjModes, NBasisAtoms, NBasisModes, J, K, BasisPermFlag,ProjPermFlag,BasisRemoveAtoms, &
                                   ProjRemoveAtoms, col, row, count
    CHARACTER(LEN=500)          :: BasisMoleculefchk, ProjMoleculefchk, BasisMoleculelog, ProjMoleculelog, BasisPermutationFile, &
                                   ProjPermutationFile, BasisRemoveAtomsFile, ProjRemoveAtomsFile
    
    ProjPermFlag = 0
    BasisPermFlag = 0
    
    
    CALL READ_COMMAND_LINE(BasisMoleculefchk, ProjMoleculefchk, BasisMoleculelog, ProjMoleculelog)
    
    CALL USER_OPTIONS_PROMPT(BasisPermutationFile, BasisPermFlag, ProjPermutationFile, ProjPermFlag, BasisRemoveAtomsFile, BasisRemoveAtoms, ProjRemoveAtomsFile, ProjRemoveAtoms)
    
    !Read in number of atoms from each fchk file
    
    NBasisAtoms = ReadNAtoms(BasisMoleculefchk)
    NBasisModes = 3*NBasisAtoms - 6 
    NProjAtoms = ReadNAtoms(ProjMoleculefchk)
    NProjModes = 3*NProjAtoms - 6 
    
    IF (NBasisAtoms > NProjAtoms) THEN
        WRITE(6,*) 'ERROR'
        WRITE(6,*) 'Basis molecule has more atoms than projected molecule'
        WRITE(6,*) 'ViPA ABORTS'
        STOP
    END IF
    
    WRITE(6,'(I0, A24)') NBasisModes, ' Modes in Basis Molecule'
    WRITE(6,'(I0, A28)') NProjModes, ' Modes in Projected Molecule'
    
    
    !Allocate and zero eigenvalue arrays, Hessian and Eigenvector Matrices
    !Note MBVectors must be the same length as MPvectors but contain zeroes in the extra elements
    Allocate(MBFrequencies(3*NBasisAtoms), MBHessian(3*NBasisAtoms,3*NBasisAtoms), MBVectors(3*NProjAtoms,3*NProjAtoms),MBMasses(NBasisAtoms), deficit(NProjAtoms*3)) 
    Allocate(MPFrequencies(3*NProjAtoms), MPHessian(3*NProjAtoms,3*NProjAtoms), MPVectors(3*NProjAtoms,3*NProjAtoms),MPMasses(NProjAtoms))
    MBFrequencies = 0.0_8
    MBVectors     = 0.0_8
    MBHessian     = 0.0_8
    MPFrequencies = 0.0_8
    MPvectors     = 0.0_8
    MPHessian     = 0.0_8
    
    !Read in Hessian from Basis Molecule fchk file
    CALL ReadHessian(BasisMoleculefchk, MBHessian)
    
    !Read in Hessian from Projection Molecule fchk file
    CALL ReadHessian(ProjMoleculefchk, MPHessian)
    
    !Read in Atomic masses from Basis Molecule log file 
    CALL ReadMasses(BasisMoleculelog, MBMasses, NBasisAtoms)
    
    !Read in Atomic masses from Projection Molecule log file 
    CALL ReadMasses(ProjMoleculelog, MPMasses, NProjAtoms)
    
    WRITE(6,*) '-- Input Files Read --'
    
    !Permute Atomic Labels
    
    IF (ProjPermFlag == 1) CALL PermuteLabels(BasisPermutationFile,MBHessian,MBMasses)
    IF (BasisPermFlag == 1) CALL PermuteLabels(ProjPermutationFile,MPHessian,MPMasses)
    IF (BasisPermFlag == 1 .OR. ProjPermFlag == 1) WRITE(6,*) '-- Labels Permuted --'
    
    !Mass Weight the Hessian 
    !Gives normalised eigenvectors
    
    CALL MassWeight(MBHessian,MBMasses)
    CALL MassWeight(MPHessian,MPMasses)

    !OPEN(77,FILE = 'BASIS_HESSIAN.dat', STATUS = 'UNKNOWN')

    !DO J = 1, NBasisModes
    !    DO K = 1, NBasisModes
    !        WRITE(77, '(E15.7)',advance='no') MBHessian(J, K)
    !    END DO
    !    WRITE(77,*)
    !END DO

    !CLOSE(77)
    
    WRITE(6,*) '-- Hessians Mass Weighted --'
    
    
    !Diagonalise Hessian Matrices
    
    CALL DIAGONALISE('U',MBHessian, MBVectors, MBFrequencies)
    CALL DIAGONALISE('U',MPHessian, MPVectors, MPFrequencies)

    ! Multiply eigvecs by diagonal matrices containing masses of atoms
    !mi_x mi_y mi_z, mi+1_x ...
    ALLOCATE(massmat(NBasisAtoms*3,NBasisAtoms*3))
    massmat = 0.0_8
    count = 0
    DO row = 1, NBasisAtoms*3, 3
        count = count + 1
        massmat(row, row) = 1.0_8/SQRT(MBMasses(count))
        massmat(row + 1, row + 1) = 1.0_8/SQRT(MBMasses(count))
        massmat(row + 2, row + 2) = 1.0_8/SQRT(MBMasses(count))
    END DO


    MBVectors = ALT_MATMUL(massmat, MBVectors)

    DO col = 1, SIZE(MBVectors,1)
        MBVectors(:,col) =  MBVectors(:,col)/SUM(MBVectors(:,col)**2)
    END DO

    
    WRITE(6,*) '-- Hessians Diagonalised --'

    
    !Remove atoms from vectors
    
    IF (BasisRemoveAtoms == 1) THEN
        CALL REMOVE_ATOMS(MBVectors,BasisRemoveAtomsFile)
    END IF
    
    IF (ProjRemoveAtoms == 1) THEN
        CALL REMOVE_ATOMS(MPVectors,ProjRemoveAtomsFile)
    END IF

    CALL WRITE_VECTORS(MBVectors, 'B')
    CALL WRITE_VECTORS(MPVectors, 'P')


    ! Convert frequencies to cm -1
    MBFrequencies = dsqrt(MBFrequencies) / (2.0_8*pi*c)
    MPFrequencies = dsqrt(MPFrequencies) / (2.0_8*pi*c)
    
    WRITE(6,*) '-- Frequencies Converted --'
    
    CALL WRITE_FREQUENCIES(MBFrequencies, 'B')
    CALL WRITE_FREQUENCIES(MPFrequencies, 'P')

    !Perform Projection
    
    CALL CALCULATE_PROJECTION(MBVectors, MPVectors, Coeff)
    
    WRITE(6,*) '-- Modes projected --'

    !Calculate deficit Factors
    DO J = 1, NProjAtoms*3
        deficit(J) = 100 - SUM(Coeff(:,J) **2 *100.0_8)
    END DO


    !Build approximate vectors from linear combination of basis vectors using coefficents from projection
    ALLOCATE(ApproxProjVectors(3*NProjAtoms,3*NProjAtoms))
    ApproxProjVectors = 0.0_8

    !Build up using contributions from each basis vector
    DO J = 1, 3*NProjAtoms
        DO K = 1,3*NBasisAtoms
            ApproxProjVectors(:,J) = ApproxProjVectors(:,J) + MBVectors(:,K)*Coeff(K,J)**2
        END DO
    END DO

    !Calculate cosine based deficit factor
    !Defined as cosine between approximate and true vectors of projection molecule
    ALLOCATE(Cosdeficit(3*NProjAtoms),Renormalised_Coeff(3*NProjAtoms,3*NProjAtoms), angles(3*NProjAtoms))
    Cosdeficit = 0.0_8
    angles = 0.0_8
    DO J = 1,3*NProjAtoms
        Cosdeficit(J) = DOT_PRODUCT(ApproxProjVectors(:,J),MPVectors(:,J))
        Cosdeficit(J) = Cosdeficit(J)/(NORM2(ApproxProjVectors(:,J))*NORM2(MPVectors(:,J)))
        Angles(J)     = ACOS(Cosdeficit(J)) * 180.0_8/pi
        Cosdeficit(J) = ABS(Cosdeficit(J) - 1) / 2
        Cosdeficit(J) = Cosdeficit(J) * 100.0_8
    END DO



    !Renormalise coefficients
    Renormalised_Coeff = 0.0_8

    DO J = 1, NProjAtoms*3
        Renormalised_Coeff(:,J) = sqrt(coeff(:,J)**2/SUM(coeff(:,J)**2))
    END DO
    
    !Write projected Modes
    
    OPEN(66, FILE = 'Expansion_Coefficients.dat', STATUS = 'UNKNOWN')
    
    WRITE(66,'(A, A, A, A, A)') 'Coefficients for ', TRIM(ADJUSTL(ProjMoleculelog)), ' (A),  written in the basis of ', TRIM(ADJUSTL(BasisMoleculelog)), ' (B)'
    WRITE(66,*)
    WRITE(66,*)
    
    
    DO J = 7, 3*NProjAtoms
        WRITE(66,'(A, I0)') 'A Mode ', J - 6
        WRITE(66,*) '------------------'
        DO K = 7, 3*NBasisAtoms
            IF (Coeff(K,J) **2 *100.0_8 > 0.1_8) WRITE(66,'(A, I0, A, F5.1)') 'B Mode ', K - 6, '  ',  (Coeff(K,J) **2 *100.0_8)
        END DO
        WRITE(66,*)
        WRITE(66,'(A, F5.1)') 'Total of coefficients = ', SUM(Coeff(:,J)**2 *100.0_8)
        WRITE(66,*)
    END DO
    
    CLOSE(66)
    
    OPEN(66, FILE = 'Biggest_Coefficient.dat', STATUS = 'UNKNOWN')
    
    WRITE(66,'(A, A, A, A, A)') 'Largest contribution to the modes of ', TRIM(ADJUSTL(ProjMoleculelog)), ' (A), by the modes of ', TRIM(ADJUSTL(BasisMoleculelog)), ' (B)'
    WRITE(66,*)
    WRITE(66,*)
    
    
    DO J = 7, 3*NProjAtoms
        WRITE(66,'(A, I0)') '(A) Vibrational Mode ', J - 6
        WRITE(66,*) '------------------'
    
            WRITE(66,'(A, I0, A, F5.1)') 'B Mode ', MAXLOC(Coeff(:,J) **2 *100.0_8,1) - 6, '  ',  MAXVAL(Coeff(:,J) **2 *100.0_8)
    
        WRITE(66,*)
    END DO
    
    CLOSE(66)
    
    OPEN(66, FILE = 'deficit.dat', STATUS = 'UNKNOWN')
    
    WRITE(66,'(A, A, A, A)') 'deficit Factors for ', TRIM(ADJUSTL(ProjMoleculelog)), ' projected onto the basis of ', TRIM(ADJUSTL(BasisMoleculelog))
    WRITE(66,*)
    WRITE(66,*)
    
    
    DO J = 7, 3*NProjAtoms
            WRITE(66,'(A, I0, A, F5.1)') 'Vibrational Mode ', J - 6, ' deficit = ', deficit(J)
    END DO
    
    CLOSE(66)

    OPEN(66, FILE = 'Cosine_deficit.dat', STATUS = 'UNKNOWN')
    
    WRITE(66,'(A, A, A, A)') 'Cosine deficit Factors for ', TRIM(ADJUSTL(ProjMoleculelog)), ' projected onto the basis of ', TRIM(ADJUSTL(BasisMoleculelog))
    WRITE(66,*)
    WRITE(66,*)
    
    
    DO J = 7, 3*NProjAtoms
            WRITE(66,'(A, I0, A, F5.1, A, F5.1)') 'Vibrational Mode ', J - 6, ' deficit = ', Cosdeficit(J), ' angle = ', angles(J)
    END DO
    
    CLOSE(66)
    
    
    OPEN(66, FILE = 'Basis_to_projected.dat', STATUS = 'UNKNOWN')
    
    write(66,'(A,I0,A)')'Printing if Contribution >1%'
    
    DO K = 1, 3*NBasisAtoms
    
    write(66,'(A,I0,A)')'Basis Mode ',K - 6, ' is present in Projection modes:'
    write(66,*)
    
    
    DO J = 1, 3*NProjAtoms
        IF (Coeff(K,J) **2 *100.0_8 > 1) THEN
            write(66,*) J-6, Coeff(K,J) **2 *100.0_8
        END IF
    END DO
    
    write(66,*)
    
    END DO
    CLOSE(66)
    
    OPEN(66, FILE = 'Biggest_Mode_Contributions.dat', STATUS = 'UNKNOWN')
    
    WRITE(66,'(A, A, A, A, A)') 'Mode of ', TRIM(ADJUSTL(ProjMoleculelog)), ' (A), to which each mode of ', TRIM(ADJUSTL(BasisMoleculelog)),' (B) contributes the most '
    WRITE(66,*)
    WRITE(66,*)
    
    
    DO K = 1, 3*NBasisAtoms
    
        WRITE(66,'(A, I0)') '(B) Vibrational Mode ', K - 6
        WRITE(66,*) '------------------'
    
            WRITE(66,'(A, I0, A, F5.1)') 'A Mode ', MAXLOC(Coeff(K,:) **2 *100.0_8,1) - 6, '  ',  MAXVAL(Coeff(K,:) **2 *100.0_8)
    
        WRITE(66,*)
    
    END DO
    CLOSE(66)
    
    
    OPEN(66, FILE = 'Renormalised_Expansion_Coefficients.dat', STATUS = 'UNKNOWN')
    
    WRITE(66,'(A, A, A, A, A)') 'Renormalised Coefficients for ', TRIM(ADJUSTL(ProjMoleculelog)), ' (A),  written in the basis of ', TRIM(ADJUSTL(BasisMoleculelog)), ' (B)'
    WRITE(66,*)
    WRITE(66,*)
    
    
    DO J = 7, 3*NProjAtoms
        WRITE(66,'(A, I0)') 'A Mode ', J - 6
        WRITE(66,*) '------------------'
        DO K = 7, 3*NBasisAtoms
            IF (Renormalised_Coeff(K,J) **2 *100.0_8 > 0.1_8) WRITE(66,'(A, I0, A, F5.1)') 'B Mode ', K - 6, '  ', (Renormalised_Coeff(K,J) **2 *100.0_8)
        END DO
        WRITE(66,*)
        WRITE(66,'(A, F5.1)') 'Total of coefficients = ', SUM(Renormalised_Coeff(:,J)**2 *100.0_8)
        WRITE(66,*)
    END DO
    
    CLOSE(66)
    
    OPEN(66, FILE = 'Renormalised_Biggest_Coefficient.dat', STATUS = 'UNKNOWN')
    
    WRITE(66,'(A, A, A, A, A)') 'Largest renormalised contribution to the modes of ', TRIM(ADJUSTL(ProjMoleculelog)), ' (A), by the modes of ', TRIM(ADJUSTL(BasisMoleculelog)), ' (B)'
    WRITE(66,*)
    WRITE(66,*)
    
    
    DO J = 7, 3*NProjAtoms
        WRITE(66,'(A, I0)') '(A) Vibrational Mode ', J - 6
        WRITE(66,*) '------------------'
    
            WRITE(66,'(A, I0, A, F5.1)') 'B Mode ', MAXLOC(Renormalised_Coeff(:,J) **2 *100.0_8,1) - 6, '  ',  MAXVAL(Renormalised_Coeff(:,J) **2 *100.0_8)
    
        WRITE(66,*)
    END DO
    
    CLOSE(66)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
CONTAINS

SUBROUTINE READ_COMMAND_LINE(BasisMoleculefchk, ProjMoleculefchk, BasisMoleculelog, ProjMoleculelog)
    IMPLICIT NONE
    CHARACTER(LEN = 500) :: BasisMoleculefchk, ProjMoleculefchk, BasisMoleculelog, ProjMoleculelog

    CALL GET_COMMAND_ARGUMENT(1,BasisMoleculefchk)
    
    IF (BasisMoleculefchk == '' .OR. BasisMoleculefchk == '-h') THEN
        WRITE(6,*) "Projects Normal Modes of Molecule 2 onto the basis of Molecule 1's Normal modes"
        WRITE(6,*) 
        WRITE(6,*) 'ViPA BasisMolecule.fchk ProjectedMolecule.fchk BasisMolecule.log ProjectedMolecule.log '
        STOP
    END IF
    
    CALL GET_COMMAND_ARGUMENT(2,ProjMoleculefchk)
    
    CALL GET_COMMAND_ARGUMENT(3,BasisMoleculelog)
    
    CALL GET_COMMAND_ARGUMENT(4,ProjMoleculelog)

END SUBROUTINE READ_COMMAND_LINE




SUBROUTINE USER_OPTIONS_PROMPT(BasisPermutationFile, BasisPermFlag, ProjPermutationFile, ProjPermFlag, BasisRemoveAtomsFile, BasisRemoveAtoms, ProjRemoveAtomsFile, ProjRemoveAtoms)
    IMPLICIT NONE
    CHARACTER(LEN = 1)   :: yn
    CHARACTER(LEN = 500) :: BasisPermutationFile, ProjPermutationFile, BasisRemoveAtomsFile, ProjRemoveAtomsFile
    INTEGER              :: BasisPermFlag, ProjPermFlag, BasisRemoveAtoms, ProjRemoveAtoms

    BasisRemoveAtoms = 0
    ProjRemoveAtoms = 0


    write(6,*) 'Would you like to permute the basis molecule labels? y/n'
    read(5,*) yn
    if (yn /= 'n' .AND. yn /='y') then
        yn = 'n'
            BasisPermutationFile = 'NoPermutation'
    end if
    if (yn == 'y') then
        BasisPermFlag = 1
        write(6,*) 'please give basis molecule label file'
        write(6,*) 'file format is oldlabel newlabel in columns'
        read(5,*) BasisPermutationFile
    end if
    
    write(6,*) 'Would you like to permute the projection molecule labels? y/n'
    read(5,*) yn
    if (yn /= 'n' .AND. yn /='y') then
        yn = 'n'
        ProjPermutationFile = 'NoPermutation'
    end if
    if (yn == 'y') then
        ProjPermFlag = 1
        write(6,*) 'please give projection molecule label file'
        write(6,*) 'file format is oldlabel newlabel in columns'
        read(5,*) ProjPermutationFile
    end if
    
    write(6,*) 'Would you like to remove atoms from the basis molecule ? y/n'
    read(5,*) yn
    if (yn /= 'n' .AND. yn /='y') yn = 'n'
    if (yn == 'y') then 
        write(6,*) 'please give file listing atoms to be removed'
        write(6,*) 'first line should be the number of removed atoms'
        read(5,*) BasisRemoveAtomsFile
        BasisRemoveAtoms = 1
    end if
    
    write(6,*) 'Would you like to remove atoms from the projection molecule ? y/n'
    read(5,*) yn
    if (yn /= 'n' .AND. yn /='y') yn = 'n'
    if (yn == 'y') then 
        write(6,*) 'please give file listing atoms to be removed'
        write(6,*) 'first line should be the number of removed atoms'
        read(5,*) ProjRemoveAtomsFile
        ProjRemoveAtoms = 1
    end if

    
END SUBROUTINE USER_OPTIONS_PROMPT


SUBROUTINE ReadHessian(InputFile, HessianMatrix)
    IMPLICIT NONE
    REAL(KIND = 8)              :: HessianMatrix(:,:)
    REAL(KIND = 8), ALLOCATABLE :: HessianLine(:), THessian(:,:)
    INTEGER                     :: NAtoms, LineIndex, J, NModes, NumEl, NLines, A, B
    CHARACTER(LEN = 500)        :: InputFile, Line

    NAtoms = ReadNAtoms(InputFile)

    NModes = 3*NAtoms - 6
    NumEl = ((3*NAtoms)**2 + 3*NAtoms)/2
    ALLOCATE(HessianLine(NumEl))
    ALLOCATE(THessian(NAtoms*3, NAtoms*3))
    NLines = NumEl/5
    IF (Modulo(NumEl, 5) > 0) NLines = NLines + 1

    !Read in modes from file into array format
    OPEN(33,FILE = InputFile, STATUS = 'OLD')

    LineIndex = 1
    
    !Read in quantites 5 at a time while checking if this is the final line (i.e. the line with less than 5 entries)
    DO WHILE(.true.)
        READ(33,'(A)', END = 98) Line   
        Line = adjustl(Line)

        !Hessian Matrix
        IF (Line(1:25) == 'Cartesian Force Constants') THEN
            DO J = 1, NLines
                    READ(33,*) HessianLine(LineIndex : LineIndex + RemEntries(LineIndex, NumEl))
                    LineIndex = LineIndex + 5  
            END DO
        END IF

    END DO
    98 CONTINUE 
    CLOSE(33)

    !Convert HessianLine into Upper Triangular Hessian Matrix
    HessianMatrix = 0.0_8
    
    A = 1
    B = 1
    K = 1
    
    DO J = 1, 3*NAtoms
    
        HessianMatrix(J,1 : K) = HessianLine(A : B)
        A = A + K
        B = B + K + 1
        K = K + 1
    
    END DO
    
    !Recover the upper triangular
    THessian = TRANSPOSE(HessianMatrix)
    
    DO J = 1, 3*NAtoms
        THessian(J,J) = 0.0_8
    END DO
    
    HessianMatrix = HessianMatrix + THessian
    

END SUBROUTINE ReadHessian

SUBROUTINE ReadMasses(InputFile, Masses, NAtoms)
    IMPLICIT NONE
    REAL(KIND = 8) :: Masses(:)
    INTEGER :: NAtoms, Idummy, J
    CHARACTER(LEN = 500) :: InputFile, Line, CDummy

    !Read in modes from file into array format
    OPEN(33,FILE = InputFile, STATUS = 'OLD')
    
    
    !Read in quantites 5 at a time while checking if this is the final line (i.e. the line with less than 5 entries)
    DO WHILE(.true.)
        READ(33,'(A)', END = 98) Line
        Line = adjustl(Line)
        !Masses Matrix
        IF (Line(1:19) == '- Thermochemistry -') THEN
            READ(33,*)
            READ(33,*)
            DO J = 1, NAtoms

                READ(33,*) CDummy, Idummy, CDummy, CDummy, CDummy, Idummy, CDummy, CDummy, Masses(J)
            END DO
        END IF
    END DO
    98 CONTINUE 
    CLOSE(33)


END SUBROUTINE ReadMasses


SUBROUTINE PermuteLabels(PermutationFile, Hessian, Masses)
    IMPLICIT NONE
    CHARACTER(Len = 500) :: PermutationFile
    REAL(KIND = 8)       :: Hessian(:,:), Masses(:)
    INTEGER, ALLOCATABLE :: Perm(:,:), Perm3(:,:), OldLabels(:), NewLabels(:)
    INTEGER              :: NAtoms, J
    
    NAtoms = SIZE(Masses)
    
    Allocate(OldLabels(NAtoms), NewLabels(NAtoms), Perm(NAtoms, Natoms), Perm3(3*NAtoms,3*NAtoms))
    OldLabels = 0
    NewLabels = 0
    Perm = 0
    Perm3 = 0
    
    
    OPEN(33,FILE = PermutationFile, STATUS = 'OLD')
    
    DO J = 1, NAtoms
        READ(33,*) OldLabels(J), NewLabels(J)
    END DO
    
    CLOSE(33)
    
    !Create NxN permutation matrix
    DO J = 1, NAtoms
        Perm(OldLabels(J),NewLabels(J)) = 1
    END DO
    
    
    IF (SUM(Perm) /= NAtoms) THEN
        WRITE(6,*) 'Permutation File Incorrect!'
        WRITE(6,*) 'ViPA ABORTS'
        STOP
    END IF
    
    !Create 3Nx3N permutation matrix
       
    
    Do J = 1,NAtoms
        Perm3(3*OldLabels(J) - 2, 3*NewLabels(J) - 2) = 1
        Perm3(3*OldLabels(J) - 1, 3*NewLabels(J) - 1) = 1
        Perm3(3*OldLabels(J), 3*NewLabels(J)) = 1
    END DO    
    
    Hessian = MATMUL(MATMUL(TRANSPOSE(Perm3),Hessian),Perm3)
    Masses = MATMUL(Masses,Perm)
    
END SUBROUTINE PermuteLabels







SUBROUTINE MassWeight(Hessian, Masses)

    IMPLICIT NONE
    REAL(KIND = 8)              :: Hessian(:,:), Masses(:)
    REAL(KIND = 8), ALLOCATABLE :: TripleMass(:)
    INTEGER                     :: MatrixOrder, J, K
    
    MatrixOrder = SIZE(Hessian,1)
    
    !Triple Length of mass matrix as hessian is written
    ! X1 Y1 Z1 X2 Y2 Z2 X3 Y3 Z3 
    ! M1 M1 M1 M2 M2 M2 M3 M3 M3
    
    K = 1
    
    Masses = Masses * (1.66053892E4/9.10938291)
    
    ALLOCATE(TripleMass(MatrixOrder))
    
    DO J = 1, MatrixOrder/3
        TripleMass(K:K + 2) = Masses(J)
        K = K + 3
    END DO
    
    
    
    
    DO J = 1, MatrixOrder
        DO K = 1, MatrixOrder
    
            Hessian(J,K) = Hessian(J,K) * 1.0_8/(dsqrt(TripleMass(J)*TripleMass(K)))
    
        END DO
    END DO
    
    
END SUBROUTINE MassWeight




FUNCTION RemEntries(EntNo, TotEntries) !Works out how many entries are left and tacks that on to the read statement
    IMPLICIT NONE
    INTEGER :: TotEntries, NLOEntries, RemEntries, EntNo

    NLOEntries = MODULO(TotEntries,5) !Number of left over entries on final line - 0 if 5 entries on final line

    !Checks if set of entries in file is the last one in the list
    IF (EntNo + 4 <= TotEntries) THEN
        RemEntries = 4 ! 5 entries left so add 4 to current entry number 
    ELSE
    RemEntries = TotEntries - EntNo  !<5 entries left so add remaining entries to current entry number
    END IF! 

END FUNCTION RemEntries

FUNCTION ReadNAtoms(MolFile)

    !Reads number of atoms from .fchk file

    IMPLICIT NONE
    CHARACTER(LEN=  500) :: Line, MolFile
    INTEGER :: NAtoms, ReadNAtoms, AtomFlag

    AtomFlag = 0

OPEN(33, FILE  = MolFile, STATUS = 'OLD')

    DO WHILE(.true.)
        READ(33,'(A)', END = 99) Line
        Line = adjustl(Line)
        IF (Line(1:15) == 'Number of atoms') THEN
            READ(Line(45:),*) NAtoms
            AtomFlag = 1
        END IF    
    END DO
    99 CONTINUE 
    CLOSE(33)

    IF (AtomFlag == 0) THEN
        WRITE(6,*) 'How many Atoms in ', TRIM(ADJUSTL(MolFile)),'?'
        READ(5,*) NAtoms
    END IF


    ReadNAtoms = NAtoms





END FUNCTION ReadNAtoms

SUBROUTINE REMOVE_ATOMS(Vectors, RemovalFile)
    IMPLICIT NONE
    !Removes atoms from vectors then writes over vectors
    REAL(KIND = 8)       :: Vectors(:,:)
    CHARACTER(LEN = 500), INTENT(IN) :: RemovalFile
    INTEGER              :: NRemAtoms, J
    INTEGER, ALLOCATABLE :: RemovedAtoms(:)

    !Read Removal File
    OPEN(33, file = ADJUSTL(TRIM(RemovalFile)))
        READ(33,*) NRemAtoms
        ALLOCATE(RemovedAtoms(NRemAtoms))
        DO J = 1, NRemAtoms
            READ(33,*) RemovedAtoms(J)
        END DO
    CLOSE(33)

    !Zero Rows of removed atoms

    DO J = 1, SIZE(vectors,1)
        IF (searchallint(J,RemovedAtoms) == 1) THEN
            !nhits = nhits + 1
            vectors(3*J, :)     = 0.0_8
            vectors(3*J - 1, :) = 0.0_8
            vectors(3*J - 2, :) = 0.0_8
        END IF
    END DO

!write(6,*) nhits

END SUBROUTINE REMOVE_ATOMS

function searchallint(element, vector)
    !Checks if element appears in vector
    IMPLICIT NONE
    integer :: element, vector(:), searchallint, J

    searchallint = 0

    do j = 1, size(vector)
        if (element == vector(J)) then
            searchallint = 1
            exit
        end if
    end do


end function searchallint


SUBROUTINE WRITE_FREQUENCIES(Frequencies, Molecule)
    IMPLICIT NONE
    REAL(KIND = 8)     :: Frequencies(:)
    INTEGER            :: J
    CHARACTER(LEN = 1) :: Molecule


    IF (Molecule == 'P') OPEN(77, FILE = 'Projected_Frequencies.dat', STATUS = 'UNKNOWN')
    IF (Molecule == 'B') OPEN(77, FILE = 'Basis_Frequencies.dat', STATUS = 'UNKNOWN')

    DO J = 1, SIZE(Frequencies)
        WRITE(77,*) Frequencies(J)
    END DO

    CLOSE(77)
END SUBROUTINE WRITE_FREQUENCIES

SUBROUTINE WRITE_VECTORS(Vectors, Molecule)
    IMPLICIT NONE
    REAL(KIND = 8), INTENT(IN)     :: Vectors(:,:)
    INTEGER            :: J, row
    CHARACTER(LEN = *), INTENT(IN) :: Molecule
    CHARACTER(LEN = 10) :: FMT

    WRITE(FMT,'(A,I0,A)') '(',SIZE(Vectors,2),'F15.7)'


    IF (Molecule == 'P') OPEN(77, FILE = 'Projected_Vectors.dat', STATUS = 'UNKNOWN')
    IF (Molecule == 'B') OPEN(77, FILE = 'Basis_Vectors.dat', STATUS = 'UNKNOWN')

    DO J = 1, SIZE(Vectors, 1)
        WRITE(77,TRIM(FMT)) (Vectors(row,J), row = 1, SIZE(Vectors, 1))
    END DO

    CLOSE(77)
END SUBROUTINE WRITE_VECTORS

FUNCTION INV(MATRIX)
    IMPLICIT NONE
    COMPLEX(KIND=8), DIMENSION(:,:), INTENT(IN) :: MATRIX
    COMPLEX(KIND=8), DIMENSION(SIZE(MATRIX,1),SIZE(MATRIX,2)) :: INV
    COMPLEX(KIND=8), DIMENSION(SIZE(MATRIX,1)) :: WORK  
    INTEGER, DIMENSION(SIZE(MATRIX,1)) :: PIVOT   ! PIVOT INDICES
    INTEGER :: N, INFO


    INV = MATRIX
    N = SIZE(MATRIX,1)

    CALL ZGETRF(N, N, INV, N, PIVOT, INFO) !DOES LU DECOMPOSITION
    IF (INFO < 0) THEN
        WRITE(6,'(A8,I2,A11)')'FAIL AT ',INFO,'TH ARGUMENT'
        WRITE(6,*)'FAIL AT LU DECOMPOSITION'
        STOP
    ELSE IF (INFO > 0) THEN
        WRITE(6,'(A2,I3,A1,I3,A17)')'U(',INFO,',',INFO,') IS EXACTLY ZERO'
        WRITE(6,*)'U = UPPER TRIANGULAR'
        WRITE(6,*)'MATRIX IS SINGULAR'
        WRITE(6,*)'FAIL AT LU DECOMPOSITION'
        STOP
    END IF

    CALL ZGETRI(N, INV, N, PIVOT, WORK, N, INFO) !WORKS OUT INVERSE FROM LU
    IF (INFO /= 0) then
        WRITE(6,*)'MATRIX INVERSION FAILED'
        STOP
    END IF
END FUNCTION INV


SUBROUTINE CALCULATE_PROJECTION(MBVectors, MPVectors, Coeff)
    IMPLICIT NONE
    REAL(KIND = 8)             , INTENT(IN) :: MBVectors(:,:), MPVectors(:,:)
    REAL(KIND = 8), ALLOCATABLE, INTENT(OUT) :: Coeff(:,:)
    INTEGER                                 :: J,K

    Allocate(Coeff(SIZE(MBVectors,1),SIZE(MPVectors,1))) !Allocate coefficient matrix as (Basis by Projection)

    Coeff = 0.0_8

    DO J = 1 , SIZE(MPVectors,1)
        DO K = 1 , SIZE(MBVectors,1)
            Coeff(K,J) = DOT_PRODUCT(MPVectors(:,J),MBVectors(:,K)) 
        END  DO
    END DO

END SUBROUTINE CALCULATE_PROJECTION



END PROGRAM ViPa
