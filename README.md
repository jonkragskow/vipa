# ViPA

## Description

Vibrational Projectional Analysis (ViPA) - a reproduction and extension of Grafton and Wheeler's ViPA program detailed in 

[Grafton, A. K., Wheeler, R. A. (1998). Vibrational projection analysis: New tool for quantitatively comparing vibrational normal modes of similar molecules. Journal of Computational Chemistry, 19(14), 1663–1674.](https://onlinelibrary.wiley.com/doi/abs/10.1002/%28SICI%291096-987X%2819981115%2919%3A14%3C1663%3A%3AAID-JCC11%3E3.0.CO%3B2-H)

## Theory

The normal modes $`\mathbf{Q}`$ obtained from diagonalising the force constant matrices of two different molecules $`a`$ and $`b`$ form two complete and orthonormal bases $`\{\mathbf{Q^a}\}`$, $`\{\mathbf{Q^b}\}`$. In order to quantify the similarity between the two sets of normal modes, the set $`\{\mathbf{Q^a}\}`$ can be projected onto $`\{\mathbf{Q^b}\}`$ provided that $`a`$ has a greater number of modes than $`b`$. Then the $`i`$th mode of $`a`$ can be expressed as a linear combination of the $`3N_{\text{atoms}}`$ modes of $`b`$.

```math
\mathbf{Q^a}_i = \sum_{j=1}^{3N_{\text{atoms}_b}} c_{ij}\mathbf{Q^b}_j
```

Where the expansion coefficients are defined as

```math
c_{ij} = \mathbf{Q^a}_i^{\text{T}} \cdot \mathbf{Q^b}_j
```

and describe the contributions of the $`j`$th normal mode of $`b`$ to the $`i`$th normal mode of $`a`$. For modes which cannot be expressed in this new basis, Grafton and Wheeler define the deficit factor $`d_i^a`$ as the percentage 

```math
d_i^a = 100 \left(1-\sum_{j=1}^{3N_{\text{atoms}_b}} c_{ij}^2 \right)
```

ViPA calculates the coefficients and deficit factors using the normal modes of a pair of molecules, but also extends this treatment to the $`3N_{\text{atoms}}-6`$ vibrational modes obtained using the Eckart-Sayvetz conditions as outlined in the [Gaussian whitepaper on vibrational analysis](https://gaussian.com/wp-content/uploads/dl/vib.pdf).


## Installation

### General

The ViPA source code is located in `src\vipa.py`. A `requirements.txt` file and Makefile are given for convenience. The `Makefile` is nothing more than a glorified shell script which copies `vipa` to `/usr/local/bin`.

### Manchester - CSF Instructions

Navigate to your home folder

```
cd
```

Open the file `.bashrc` in `nano`

```
nano .bashrc
```

and add the following lines to the bottom of the file

```
module load apps/git/2.19.0
module load apps/binapps/anaconda3/2019.07
module load compilers/intel/18.0.3
module load tools/env/proxy
```

If these lines are already present then you do *not* need to add them again.

To save and exit `nano` press CTRL+o, press Enter (or return), and then press CTRL+x.

Then `source` this file with

```
source .bashrc
```
If you do not already have a `git` folder in your csf home, make one

```
mkdir git
```

The same goes for a `bin` folder

```
mkdir bin
```

Navigate to your `git` folder 

```
cd git
```

and clone this repository - this gives you your own version which you can use and modify if you need to.

```
git clone https://gitlab.com/jonkragskow/vipa
```

Then navigate to `src`

```
cd src
```

and install the tools

```
make
```

You should now be able to use ViPA on the csf, try it using

``` 
vipa -h
```

Which should print a help section

## Updating

To update the code simply type navigate to the repository and then use

```
git pull
```
then move to `src` and run `make`
```
cd src;make
```

If you get an error about local changes, first type
```
git stash
```
and repeat the above steps

## Usage

ViPA requires only two files from the user. Both are formatted checkpoint files (`.fchk`) from Gaussian and must contain the Hessian. Example files for benzene and phenol are given in `src/bp_test`.

The help argument `-h` can be used to show additional features.

An example input to ViPA is 

```
vipa molecule_1.fchk molecule_2.fchk
``` 

where the first molecule's normal/vibrational modes define a basis onto which the modes of molecule 2 are projected. Therefore molecule 1 must contain fewer atoms than molecule 2.

ViPA writes information on the normal and vibrational modes, and their corresponding projections to the output file `vipa.out`. Additionally, plots of the expansion coefficients are created for the vibrational and normal mode projections and are saved in `vipa_vibrational_heatmap.png` and `vipa_normal_heatmap.png` respectively.

## Bugs

Please report all bugs to Jon through gitlab
